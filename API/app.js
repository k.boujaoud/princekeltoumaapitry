const express = require('express');
// const axios = require('axios');
const bodyParser = require('body-parser');
const fs= require("fs");
var app = express();
// var newUser ={"users" : []}


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


app.post("/user/new", function(req,res){
    
    
    var fichierJson= require("./utilisateurs.json");
    var listeUsers = fichierJson.users;
    listeUsers.push(req.body.data);
    console.log(listeUsers);
    var userContent = "{" + '"users":' + JSON.stringify(listeUsers,null,2)+"}";
    fs.writeFile("utilisateurs.json", userContent,function (err, data){
        if(err) throw (err);
    });      
    res.send();
});

app.get("/users" , function(req,res){
    fs.readFile("utilisateurs.json",function(err,data){
        if (err) throw (err);
        var jsonArray = JSON.parse(data)
        console.log(jsonArray)
        res.json(jsonArray);
    })
    
    
})

app.delete(/users/userName)

app.listen(3000, function () {
    console.log("Votre API est bien en ligne");
});
